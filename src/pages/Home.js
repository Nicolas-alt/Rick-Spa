import getData from '../utils/getData';
import ScrollReveal from 'scrollreveal';

const Home = async() => {
        const characters = await getData();
        const view = `
    <div class="Characters">
      ${characters.results.map( character =>`
        <article class="Characters-item">
            <a href="#/${character.id}/">
                <img src="${character.image}" alt="${character.name}" />
                  <h2>${character.name}</h2>
                  <p>${character.location.name}</p>
            </a>
        </article>`
    )}
    </div>
    `;
    const card = ScrollReveal({ reset: false });
    card.reveal('.Characters-item',{
     opacity: 0,
     duration: 100,
     interval: 150,
     rotate: {x:20, y:50 }
   }, 20);
  return view;
}

export default Home;