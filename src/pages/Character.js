import getHash from '../utils/getHash';
import getData from '../utils/getData';


const Character = async() => {
        const id = getHash();
        const character = await getData(id);
        const characters = await getData();
        const { image, name, species, gender, status } = character;
        let statusColor;

        if (status === ' unknown') {
            statusColor = ` <span  class="text-warning">${status}</span>`
        } else if (status === 'Alive') {
            statusColor = ` <span  class="text-success">${status}</span>`
        } else if (status === 'Dead') {
            statusColor = ` <span  class="text-danger">${status}</span>`
        } else if (status === 'undefined') {
            statusColor = ` <span  class="text-info">${status}</span>`
        }

        const view = `
<section class="Characters-inner">
    <div class="div-Character-info animate__animated animate__fadeInUp">
      <img class="Img-Character" src="${image}" alt="${name}" />
        <h2>${name}</h2>
        <p> <i class='bx bx-map-pin'></i>
            Origin: <span>${character.origin.name}</span>
        </p>
        <p> <i class='bx bx-current-location'></i>
            Last Location: <span>${character.location.name}</span>
        </p>
        <p> <i class='bx bx-dna'></i>
            Species: <span>${species}</span>
        </p>
        <p> <i class='bx bxs-baby-carriage'></i>
            Gender: <span>${gender}</span></p>
        <p> <i class='bx bx-pulse'></i>
            Status: ${statusColor}</p>
        <p> <i class='bx bx-tv'></i>
            Episodes: <span>${character.episode.length}</span></p>
    </div>
    <div class=div-Others>
        <p>Others characteres: <a href="/">See more</a></p>
        <div class="Characters-div">
            ${characters.results.map(character => `
            <a href="#/${character.id}/">
                <img class="Characters-Img" src="${character.image}" alt="${character.name}" />
            </a>` )}
        </div>
    </div>
</section>
    `;
  return view;
}

export default Character;