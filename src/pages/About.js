const About = _ => {
    const view = `
<section class="Characters-inner">
    <div class="div-Character-info animate__animated animate__fadeInUp">
        <img class="Img-Character" src="https://avatars0.githubusercontent.com/u/52179095?s=460&u=3c32f626fcbc00f39db5da407a76ce7a0bb3e7f3&v=4" alt="Nicolas Picture" />
        <h2>Hi there!  I'm Nicolas Jiménez </h2>
        <p class="p-about">This project was made by a passionate frontend developer from Colombia 🇨🇴</p>
        <p>You can find others projects 
            <a href="https://github.com/Nicolas-alt">
            Here
            </a>
            </p>
    </div>
</section>
    `;
    return view;
}

export default About;