let page;
function random () {
    page = Math.ceil(Math.random() * 33)
  return page;
}
random()

const API = `https://rickandmortyapi.com/api/character/?page=${page}`;
const urlCharacter = "https://rickandmortyapi.com/api/character/"

const getData = async (id) => {
  const apiURL = id ? `${urlCharacter}${id}` : API;
  try {
    const respnse = await fetch(apiURL);
    const data = await respnse.json();
    return data;
  }
  catch (error) {
    console.error('Fetch Error ', error);
  }
}

export default getData;
