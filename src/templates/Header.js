const Header = () => {
    const view = `
        <ul class="Header-main">
            <li class="Header-logo">
              <a  class="Header-a" href="/">
                Morty's-Spa
              </a>
            </li>
            <li class="Header-nav">
              <a href="#/about">
                  about
              </a>
            </li>
        </ul>
    `;
    return view;
};

export default Header;